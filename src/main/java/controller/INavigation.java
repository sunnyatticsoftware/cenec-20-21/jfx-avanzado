package controller;

public interface INavigation {
    void Navigate(ScreenEnum screenEnum);
}
