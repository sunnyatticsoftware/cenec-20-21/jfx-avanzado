package controller;

import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import model.Login;

public class LoginController {
    
    @FXML
    private Button btnLogin;
    
    @FXML
    private TextField txtUsername;
    
    @FXML
    private PasswordField txtPassword;
    
    @FXML
    private Label lblMessage;
    
    private final Login login;
    private final INavigation navigation;
    
    public LoginController(INavigation navigation, Login login){
        this.navigation = navigation;
        this.login = login;
    }
    
    public void initialize(){
        this.btnLogin.disableProperty().bind(
                Bindings.or(this.txtUsername.textProperty().isEmpty(), this.txtPassword.textProperty().isEmpty()));
        
        this.btnLogin.setOnAction((actionEvent) -> {
            var username = this.txtUsername.getText();
            var password = this.txtPassword.getText();
            var isValid = this.login.isValid(username, password);
            if(isValid){
                this.navigation.Navigate(ScreenEnum.Main);
            } else {
                this.lblMessage.setText("Credenciales incorrectas");
                this.txtPassword.clear();
            }
        });
    }
}
