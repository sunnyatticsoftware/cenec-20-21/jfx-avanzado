package model;


public class InMemoryCredentialsRepository implements ICredentialsRepository {

    private final String USERNAME = "admin";
    private final String PASSWORD = "admin";

    @Override
    public String getUsername() {
        return USERNAME;
    }

    @Override
    public String getPassword() {
        return PASSWORD;
    }
    
}
