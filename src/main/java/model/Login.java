package model;

public class Login {
    private final ICredentialsRepository credentialsRepository;
    
    public Login(ICredentialsRepository credentialsRepository){
        this.credentialsRepository = credentialsRepository;
    }
    
    public boolean isValid(String username, String password){
        var adminUsername = this.credentialsRepository.getUsername();
        var adminPassword = this.credentialsRepository.getPassword();
        
        return username.equals((adminUsername)) && password.equals(adminPassword);
    }
}
