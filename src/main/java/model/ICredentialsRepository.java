package model;

public interface ICredentialsRepository {
    String getUsername();
    String getPassword();
}
