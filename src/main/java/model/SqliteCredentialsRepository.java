package model;

import javafx.application.Platform;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqliteCredentialsRepository implements ICredentialsRepository{

    private final String Url = "jdbc:sqlite:C:/sqlDatabase/cenec.db";

    @Override
    public String getUsername() {
        Connection connection = null;
        try {
            connection = openConnection();
            var statement = connection.createStatement();
            var resultSet = statement.executeQuery("SELECT username FROM User");
            if(resultSet.next()){
                var username = resultSet.getString("username");
                System.out.println("Username retrieved: " + username);
                return username;
            }
            throw new SQLException("There is no username in table User");
        } catch (SQLException exception) {
            System.out.println("Error getting username from SQLite " + exception.getMessage());
            Platform.exit();
            return null;
        } finally {
            closeConnection(connection);
        }
    }

    @Override
    public String getPassword() {
        Connection connection = null;
        try {
            connection = openConnection();
            var statement = connection.createStatement();
            var resultSet = statement.executeQuery("SELECT password FROM User");
            if(resultSet.next()){
                var password = resultSet.getString("password");
                System.out.println("Password retrieved: " + password);
                return password;
            }
            throw new SQLException("There is no password in table User");
        } catch (SQLException exception) {
            System.out.println("Error getting password from SQLite " + exception.getMessage());
            Platform.exit();
            return null;
        } finally {
            closeConnection(connection);
        }
    }

    private Connection openConnection(){
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(Url);
            return connection;
        }
        catch (SQLException  e) {
            System.out.println(e.getMessage());
            closeConnection(connection);
            Platform.exit();
            return null;
        }
    }

    private void closeConnection(Connection connection){
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
